## A bunch of header ilbraries

This library is meant to be used on Windows, but might be rather easily ported to Linux or Mac, 
if needed.

Please note that for the OpenGL library (f_gl.h) you need to have the following prerequisites 
fulfilled to use these libraries:

- create a valid OpenGL context for OpenGL version >= 4.6
- link to the GLEW extension library




