#pragma once

struct V3 {
	float x;
	float y;
	float z;

	V3 operator+ (const V3& other) {
		return { this->x + other.x, this->y + other.y, this->z + other.z };
	}

	V3 operator- (const V3& other) {
		return { this->x - other.x, this->y - other.y, this->z - other.z };
	}

	float dot(const V3& other) {
		return this->x * other.x + this->y * other.y + this->z * other.z;
	}
};

struct V4 {
	float x;
	float y;
	float z;
	float w;

	float* data() {
		return &x;
	}

	V4 operator+ (const V4& other) {
		return { this->x + other.x, this->y + other.y, this->z + other.z, this->w + other.w };
	}

	V4 operator- (const V4& other) {
		return { this->x - other.x, this->y - other.y, this->z - other.z, this->w - other.w };
	}

	float dot(const V4& other) {
		return this->x * other.x + this->y * other.y + this->z * other.z + this->w * other.w;
	}

};

struct M4 {
	M4(V4 row1, V4 row2, V4 row3, V4 row4) : r1(row1), r2(row2), r3(row3), r4(row4) 
	{
		c1 = { r1.x, r2.x, r3.x, r4.x };
		c2 = { r1.y, r2.y, r3.y, r4.y };
		c3 = { r1.z, r2.z, r3.z, r4.z };
		c4 = { r1.w, r2.w, r3.w, r4.w };

	}

	V4 r1;
	V4 r2;
	V4 r3;
	V4 r4;

	V4 c1;
	V4 c2;
	V4 c3;
	V4 c4;

	float* colData() {
		return c1.data();
	}


	V4 operator* (const V4& other) {
		V4 vr;
		vr.x = r1.x * other.x + r1.y * other.y + r1.z * other.z + r1.w * other.w;
		vr.y = r2.x * other.x + r2.y * other.y + r2.z * other.z + r2.w * other.w;
		vr.z = r3.x * other.x + r3.y * other.y + r3.z * other.z + r3.w * other.w;
		vr.w = r4.x * other.x + r4.y * other.y + r4.z * other.z + r4.w * other.w;
		return vr;
	}

	M4 operator* (const M4& other) {
		M4* t = this;
		V4 r1 = { this->r1.dot(other.c1), this->r1.dot(other.c2), this->r1.dot(other.c3), this->r1.dot(other.c4) };
		V4 r2 = { this->r2.dot(other.c1), this->r2.dot(other.c2), this->r2.dot(other.c3), this->r2.dot(other.c4) };
		V4 r3 = { this->r3.dot(other.c1), this->r3.dot(other.c2), this->r3.dot(other.c3), this->r3.dot(other.c4) };
		V4 r4 = { this->r4.dot(other.c1), this->r4.dot(other.c2), this->r4.dot(other.c3), this->r4.dot(other.c4) };

		M4 result = {
			r1,
			r2,
			r3,
			r4
		};

		return result;
	}
};

M4 f_mtrans(float x, float y, float z) {
	return {
		{1, 0, 0, x},
		{0, 1, 0, y},
		{0, 0, 1, z},
		{0, 0, 0, 1}
	};
}

M4 f_mscale(float x, float y, float z) {
	return {
		{x, 0, 0, 0},
		{0, y, 0, 0},
		{0, 0, z, 0},
		{0, 0, 0, 1}
	};
}
