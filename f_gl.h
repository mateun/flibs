#pragma once

#include <GL/glew.h>
#include <vector>
#include "f_math.h"

#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

struct FVAO {
    GLuint vaoId;
    uint32_t numberOfVertices;
};

struct FShader {
	GLuint _shaderId;
};

struct FVBuffer {
    GLuint _id;
};

struct FIBuffer {
    GLuint _id;
};

struct FGLResult {
	bool ok;
	std::string errorMessage;
};

struct FTexture {
    GLuint texId;
};


// Creates a shader program based on the transmitted sources for vertex- and fragment shader.
FGLResult f_createShader(const std::string& vertexShaderSource, const std::string& fragmentShaderSource, FShader* target);
FGLResult f_createVertexBuffer(std::vector<float> data, FVBuffer* vb);
FGLResult f_createIndexBuffer(std::vector<uint32_t> data, FIBuffer* ib);
FGLResult f_createVAO(FVAO* target);
FGLResult f_unbindVAO();
FGLResult f_setVertexAttribute(GLuint slot, GLenum type, int nrOfComponents);
FGLResult f_setMatrixUniform(GLuint slot, M4 matrix);
FGLResult f_createTextureRGBA(int w, int h, void* pixelData, FTexture* target);
FGLResult f_createFrameBuffer(int w, int h, GLuint* colorTexture, GLuint* depthTexture, GLuint* fbo);



FGLResult f_createVertexBuffer(std::vector<float> data, FVBuffer* target) {
    FGLResult result = { true, "" };

    GLuint vb;
    glGenBuffers(1, &vb);
    glBindBuffer(GL_ARRAY_BUFFER, vb);
    glBufferData(GL_ARRAY_BUFFER, data.size() * 4, data.data(), GL_STATIC_DRAW);

    GLenum err = glGetError();
    if (err != 0) {
        result.ok = false;
        result.errorMessage = glGetError();
        return result;
    }

    target->_id = vb;

    return result;
}


FGLResult f_createIndexBuffer(std::vector<uint32_t> data, FIBuffer* target) {
    FGLResult result = { true, "" };

    GLuint ib;
    glGenBuffers(1, &ib);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, data.size() * 4, data.data(), GL_STATIC_DRAW);

    GLenum err = glGetError();
    if (err != 0) {
        result.ok = false;
        result.errorMessage = glGetError();
        return result;
    }

    target->_id = ib;

    return result;
}

FGLResult f_createShader(const std::string& vertexShaderSource, const std::string& fragmentShaderSource, FShader* target) {
    FGLResult result = { true, "" };

    
        GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* vssource_char = vertexShaderSource.c_str();
        glShaderSource(vshader, 1, &vssource_char, NULL);
        glCompileShader(vshader);
        GLint compileStatus;
        glGetShaderiv(vshader, GL_COMPILE_STATUS, &compileStatus);
        if (GL_FALSE == compileStatus) {
            result.ok = false;
            GLint logSize = 0;
            glGetShaderiv(vshader, GL_INFO_LOG_LENGTH, &logSize);
            std::vector<GLchar> errorLog(logSize);
            glGetShaderInfoLog(vshader, logSize, &logSize, &errorLog[0]);
            result.errorMessage = errorLog.data();
            glDeleteShader(vshader);
            return result;

        }

    
        GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* fssource_char = fragmentShaderSource.c_str();
        glShaderSource(fshader, 1, &fssource_char, NULL);
        glCompileShader(fshader);
        
        glGetShaderiv(fshader, GL_COMPILE_STATUS, &compileStatus);
        if (GL_FALSE == compileStatus) {
            result.ok = false;
            GLint logSize = 0;
            glGetShaderiv(fshader, GL_INFO_LOG_LENGTH, &logSize);
            std::vector<GLchar> errorLog(logSize);
            glGetShaderInfoLog(fshader, logSize, &logSize, &errorLog[0]);
            result.errorMessage = errorLog.data();
            glDeleteShader(fshader);
            return result;

        }
    
        GLuint p = glCreateProgram();
        glAttachShader(p, vshader);
        glAttachShader(p, fshader);
        glLinkProgram(p);
        
        glGetProgramiv(p, GL_LINK_STATUS, &compileStatus);

        if (GL_FALSE == compileStatus) {
            GLint maxLength = 0;
            glGetProgramiv(p, GL_INFO_LOG_LENGTH, &maxLength);
            std::vector<GLchar> infoLog(maxLength);
            glGetProgramInfoLog(p, maxLength, &maxLength, &infoLog[0]);
            result.ok = false;
            result.errorMessage = infoLog.data();

            glDeleteProgram(p);
            glDeleteShader(vshader);
            glDeleteShader(fshader);

        }

        GLenum err = glGetError();
        if (err != 0) {
            result.ok = false;
            result.errorMessage = err;
            return result;
        }

        glDeleteShader(vshader);
        glDeleteShader(fshader);

        target->_shaderId = p;

        return result;

}

FGLResult f_createVAO(FVAO* target) {
    GLuint vao;
    glCreateVertexArrays(1, &vao);
    glBindVertexArray(vao);
    target->vaoId = vao;
    return { true };
}

FGLResult f_unbindVAO() {
    glBindVertexArray(0);
    return { true };
}

FGLResult f_setVertexAttribute(GLuint slot, GLenum type, int nrOfComponents) {

    glVertexAttribPointer(slot, nrOfComponents, type, GL_FALSE, 0, (void*) nullptr);
    glEnableVertexAttribArray(slot);
    return { true };


}

FGLResult f_setMatrixUniform(GLuint slot, M4 matrix) {
    glUniformMatrix4fv(slot, 1, false, matrix.colData());
    return { true };
}

FGLResult f_createTextureRGBA(int w, int h, void* pixelData, FTexture* target) {
    GLuint handle;
    glGenTextures(1, &handle);
    glBindTexture(GL_TEXTURE_2D, handle);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixelData );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBindTexture(GL_TEXTURE_2D, 0);
    target->texId = handle;

    return { true };
}

FGLResult f_createFrameBuffer(int w, int h, GLuint* colorTexture, GLuint* depthTexture, GLuint* fbo) {
    GLuint fb;
    glGenFramebuffers(1, &fb);
    glBindFramebuffer(GL_FRAMEBUFFER, fb);

    if (colorTexture != nullptr) {
        unsigned int texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, w, h);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
        *colorTexture = texture;
    }
    
    if (depthTexture) {
        unsigned int texdepth;
        glGenTextures(1, &texdepth);
        glBindTexture(GL_TEXTURE_2D, texdepth);
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, w, h);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texdepth, 0);
        *depthTexture = texdepth;
    }
    

  
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE) {
        *fbo = fb;
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return { true };
    }
    else {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        return FGLResult{ false, "Framebuffer incomplete!" + glGetError() };
    }

    

}


